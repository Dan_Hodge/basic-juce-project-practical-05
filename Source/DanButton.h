//
//  DanButton.h
//  JuceBasicWindow
//
//  Created by Dan Hodge on 06/11/2014.
//
//

#ifndef DANBUTTON_H_INCLUDED
#define DANBUTTON_H_INCLUDED

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

enum Button_Status {MOUSE_UP, MOUSE_DOWN, MOUSE_ENTER, MOUSE_EXIT};

class DanButton : public Component
{
public:
    DanButton();
    void paint(Graphics& g) override;
    void mouseUp(const MouseEvent& event);
    void mouseDown(const MouseEvent& event);
    void mouseEnter(const MouseEvent& event);
    void mouseExit(const MouseEvent& event);
    
private:
    Button_Status buttonStatus;
};



#endif /* DANBUTTON_H_INCLUDED */
