/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //addAndMakeVisible(colourSelector);
    addAndMakeVisible(danButton);
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    colourSelector.setBounds(0, 0, getWidth(), getHeight()/2);
    danButton.setBounds(0, getHeight()/2, 60, 40);
}


void MainComponent::paint(Graphics& g)
{
    
    g.setColour(colourSelector.getCurrentColour());
    g.fillEllipse(mouseX-5, mouseY-5, 10, 10);

    
}

void MainComponent::mouseUp(const MouseEvent& event)
{
    mouseX = event.x;
    mouseY = event.y;
    repaint();
}



void MainComponent::buttonClicked(Button* button)
{

}


void MainComponent::sliderValueChanged(Slider* slider)
{
    
    DBG("Value:" << slider->getValue());
}

