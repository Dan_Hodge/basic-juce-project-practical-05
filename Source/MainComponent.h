/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "DanButton.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public Button::Listener,
                        public Slider::Listener

{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized();
    
    void paint(Graphics& g) override;
    void mouseUp(const MouseEvent& event) override;
    
    void buttonClicked (Button* button) override;
    void sliderValueChanged (Slider* slider) override;

private:
    //==============================================================================
    ColourSelector colourSelector;
    DanButton danButton;
    
    int mouseX, mouseY;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
