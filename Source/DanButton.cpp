//
//  DanButton.cpp
//  JuceBasicWindow
//
//  Created by Dan Hodge on 06/11/2014.
//
//

#include "DanButton.h"


DanButton::DanButton()
{
    buttonStatus = MOUSE_EXIT;
}

void DanButton::paint(Graphics& g)
{
    
    if (buttonStatus == MOUSE_UP)
        g.setColour(Colours::tomato);
    
    if (buttonStatus == MOUSE_DOWN)
        g.setColour(Colours::peachpuff);
    
    if (buttonStatus == MOUSE_ENTER)
        g.setColour(Colours::yellowgreen);
    
    if (buttonStatus == MOUSE_EXIT)
        g.setColour(Colours::black);
    
    
    
    
    g.fillAll();
    
    
}

void DanButton::mouseUp(const MouseEvent& event)
{
    buttonStatus = MOUSE_UP;
    repaint();
}


void DanButton::mouseDown(const MouseEvent& event)
{
    buttonStatus = MOUSE_DOWN;
    repaint();
}


void DanButton::mouseEnter(const MouseEvent& event)
{
    buttonStatus = MOUSE_ENTER;
    repaint();
}


void DanButton::mouseExit(const MouseEvent& event)
{
    buttonStatus = MOUSE_EXIT;
    repaint();
}